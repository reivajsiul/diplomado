import React from 'react';
import './Menu.css';
import UNAM from '../img/UNAM.png'; 
export default function Menu(){
  return(
    <div className="flex abs menu">
      <div className="logos">
        <span className="logo1">
          <img className="logo" src={UNAM}/>
        </span>
        <div className="linea"></div>
        <span className="logo1">
          <img className="logo" src={UNAM}/>
        </span>
      </div>
      <div className="enlaces_menu">
        <ul className="flex">
          <li><a href="#">Inicio</a></li>
          <li><a href="#">Area 1 <i class="fas fa-caret-down"></i></a>
            <ul>
              <li><a href="#">Cursos</a></li>
              <li><a href="#">Diplomandos</a></li>
            </ul>  
          </li>
          <li><a href="#">Area 3 <i class="fas fa-caret-down"></i></a>
            <ul>
              <li><a href="#">Cursos</a></li>
              <li><a href="#">Diplomandos</a></li>
            </ul> 
          </li>
          <li><a href="#">Area 4 <i class="fas fa-caret-down"></i></a>
            <ul>
              <li><a href="#">Cursos</a></li>
              <li><a href="#">Diplomandos</a></li>
            </ul> 
          </li>
          <li><a href="#">Informes</a></li>
        </ul>
      </div>
    </div>
  );
}