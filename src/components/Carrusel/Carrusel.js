import React from 'react';
import Img from '../img/server.webp'; 
import Grid from '@material-ui/core/Grid';
import './Carrusel.css'

export default function Carrusel(){
  return(
    <div className="relativo">
      <Grid container>
      <Grid item md={12}>
      <div id="carouselExampleIndicators" className="carousel slide img" data-ride="carousel">
  <ol className="carousel-indicators abs flecha">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div className="carousel-inner">
    <div className="carousel-item active relativo">
      <div className="flex abs mov">
        <div className="flex titulo">
          <div className="texto_t">DIPLOMADO DE<br/>ANALISIS DE <br/>DATOS</div>
        </div>
        <div className="flex fecha">
          <div className="texto_f">Inicia 24 de Abril</div>
        </div>
      </div>
      <img className="d-block w-100" src={Img} alt="First slide"/>
      
    </div>

    <div className="carousel-item relativo">
      <div className="flex abs mov">
        <div className="flex titulo">
          <div className="texto_t">DIPLOMADO DE<br/>ADMINISTRACION <br/>DE REDES</div>
        </div>
        <div className="flex fecha">
          <div className="texto_f">Inicia 24 de Abril</div>
        </div>
      </div>
      <img className="d-block w-100" src={Img} alt="First slide"/>
      
    </div>

    <div className="carousel-item relativo">
      <div className="flex abs mov">
        <div className="flex titulo">
          <div className="texto_t">DIPLOMADO DE<br/>CLUSTERIZACION <br/>DE DATOS</div>
        </div>
        <div className="flex fecha">
          <div className="texto_f">Inicia 24 de Abril</div>
        </div>
      </div>
      <img className="d-block w-100" src={Img} alt="First slide"/>
      
    </div>
    
  </div>
  <a className="carousel-control-prev abs flecha" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
    <span className="sr-only">Previous</span>
  </a>
  <a className="carousel-control-next abs flecha" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span className="carousel-control-next-icon" aria-hidden="true"></span>
    <span className="sr-only">Next</span>
  </a>
    </div>
      </Grid>
      </Grid>
    </div>
  );
}